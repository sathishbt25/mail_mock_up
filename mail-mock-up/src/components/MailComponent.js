import React from 'react';
import {Card, ListGroup, Form, Row, Col} from 'react-bootstrap';
import StarBorderOutlined from '@material-ui/icons/StarBorderOutlined';
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import ReplyOutlined from '@material-ui/icons/ReplyOutlined';
import ArrayForwardOutlined from '@material-ui/icons/ArrowForwardOutlined';

export default class MailComponent extends React.Component {

    constructor(props) {
        super(props);
        this.mailClicked = this.mailClicked.bind(this);
        this.state = {
            data : props.dataFromApp[0].mail[0]
        }
    }
    mailClicked(selectedMail){
        this.setState({data : selectedMail});
    }
    render() {
        this.mailContent =  this.props.dataFromApp[0].mail[0];
        var mainDivStyle = {
            marginBottom: "2%",
            marginTop: "2%"
        }
        var divStyle= {
            marginLeft: "3.3%",
            marginBottom: "2%",
            marginTop: "2%"
        }
        var mailContentDivStyle= {
            marginBottom: "2%",
        }
        const listItems = this.props.dataFromApp.map((d) => {return (d.mail.map((m)=><ListGroup.Item key={m.subject} action onClick={()=>this.mailClicked(m)}><Row>
            <Col xs={1}><Form.Check type="checkbox" /></Col>
            <Col xs={8}><div>{m.subject}<br/>{m.senderName}</div></Col>
            <Col xs={2}><div>{m.date}</div></Col>
            <Col xs={1}><StarBorderOutlined/></Col>
        </Row></ListGroup.Item>))})
        return (
            <div style={mainDivStyle}>
                <div style={divStyle}>
                    <Row>
                        <Form.Check type="checkbox"/>
                        <ListGroup horizontal>
                            <ListGroup.Item><VisibilityOffOutlinedIcon/></ListGroup.Item>
                            <ListGroup.Item><DeleteOutlinedIcon/></ListGroup.Item>
                            <ListGroup.Item><RemoveCircleOutlineOutlinedIcon/></ListGroup.Item>
                        </ListGroup>
                    </Row>
                </div>
                <Row>
                    <Col xs={6}>
                        <Card>
                            <ListGroup>
                                {listItems}
                            </ListGroup>
                        </Card>
                    </Col>
                    <Col xs={6}>
                        <div style={mailContentDivStyle}>
                            <ListGroup>
                                <ListGroup.Item><Row><Col xs={10} key={this.state.data.subject}>{this.state.data.subject}</Col>
                                    <Col xs={1}><ReplyOutlined/></Col>
                                    <Col xs={1}><ArrayForwardOutlined/></Col>
                                </Row>
                                </ListGroup.Item>
                            </ListGroup>
                        </div>
                        <Card key={this.state.data.senderName}>
                            <Card.Header><Row>
                                <Col xs={9}>From: {this.state.data.senderName}<br/>To: </Col>
                                <Col xs={2}>{this.state.data.date}</Col>
                                <Col xs={1}><StarBorderOutlined/></Col>
                            </Row></Card.Header>
                            <Card.Body>{this.state.data.content}</Card.Body>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}