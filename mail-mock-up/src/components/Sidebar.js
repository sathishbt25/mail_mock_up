import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MoveToInboxOutlinedIcon from '@material-ui/icons/MoveToInboxOutlined';
import NearMeOutlinedIcon from '@material-ui/icons/NearMeOutlined';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import DraftsIcon from '@material-ui/icons/Drafts';
import {Button} from 'react-bootstrap';

function Sidebar() {
    return (

        <List>
            <ListItem button className="mr-sm-2">
                <MoveToInboxOutlinedIcon></MoveToInboxOutlinedIcon>
                <ListItemText>Inbox</ListItemText>
            </ListItem>
            <ListItem button className="mr-sm-2">
                <DraftsIcon/>
                <ListItemText>Drafts</ListItemText>
            </ListItem>
            <ListItem button className="mr-sm-2">
                <NearMeOutlinedIcon/>
                <ListItemText>Sent</ListItemText>
            </ListItem>
            <ListItem button className="mr-sm-2">
                <RemoveCircleOutlineOutlinedIcon/>
                <ListItemText>Spam</ListItemText>
            </ListItem>
            <ListItem button className="mr-sm-2">
                <DeleteOutlinedIcon/>
                <ListItemText>Trash</ListItemText>
            </ListItem>
        </List>
    )
}

export default Sidebar