import React from 'react';
import { Button, Navbar, Form, FormControl, InputGroup, Container, Row, Col, NavDropdown} from 'react-bootstrap';
import Sidebar from './components/Sidebar';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import MailOutlinedIcon from '@material-ui/icons/MailOutlined';
import MailComponent from './components/MailComponent';
import TextField from '@material-ui/core/TextField';
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";

export default class App extends React.Component {
    render() {
        var sideBarStyle = {
            height : "91vh",
            backgroundColor: "#006666",
            color: "white",
        }
        var containerStyle = {
            height: "91vh",
            maxWidth: "100%"
        }
        var navDropdownStyle = {
            float: "right"
        }
        var navBarStyle= {
            backgroundColor: "#00b3b3",
            color: "white"
        }
        var navbarBrandStyle = {
            color: "white",
            marginLeft: "1.2%"
        }
        var buttonStyle = {
            marginTop: "4%",
            marginLeft: "5%",
            backgroundColor: "#c1c1a4"
        }
        var textFieldStyle = {
            marginLeft: "10%",
            width: "30%"
        }
        this.state = {
            "accounts": [{
                "address": "g.fugger@mail.com",
                "mail": [{
                    "content": "Dear Madam, \n I am writing to you to propose a lucrative business based on block chain.\n      Send" +
                    "me your passport to confirm\nSend me your passport to confirm\nSend me your passport to confirm\nSend me your passport to confirm",
                    "date": "Dec 7",
                    "senderEmail": "r.volkwist@hotmail.com",
                    "senderName": "Robert Volkwist",
                    "subject": "Business Proposal"
                }, {
                    "content": "Dear Madam,    \n your tenant is very noise. Their music keeps me up at night!",
                    "date": "Dec 6",
                    "senderEmail": "a.jacobs@msd.com",
                    "senderName": "Aletta Jacobs",
                    "subject": "Noise from upstairs"
                }],
                "name": "Gustav",
                "surname": "Fugger"
            }]
        };
        const navBarDropdownTitle = (<PersonOutlineIcon></PersonOutlineIcon>);
        return (
            <div className="App">
                <Navbar style={navBarStyle}>
                    <Navbar.Brand style={navbarBrandStyle}><MailOutlinedIcon/>Mail</Navbar.Brand>
                    <TextField style={textFieldStyle} placeholder="Search" InputProps={{
                        endAdornment: (<InputAdornment position="start"><SearchIcon/></InputAdornment>)
                    }}>
                    </TextField>
                    <Navbar.Collapse className="justify-content-end">
                        <NavDropdown alignRight title={navBarDropdownTitle} id="collasible-nav-dropdown">
                            <NavDropdown.Item href="#">Switch Account</NavDropdown.Item>
                        </NavDropdown>
                    </Navbar.Collapse>


                </Navbar>
                <Container style={containerStyle}>
                    <Row>
                        <Col xs={2} style={sideBarStyle}><Button style={buttonStyle}>Compose</Button>
                            <Sidebar></Sidebar></Col>
                        <Col xs={10}>
                            <MailComponent dataFromApp={this.state.accounts}/>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
